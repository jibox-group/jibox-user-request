import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.scss']
})
export class AsideComponent implements OnInit {

  link: string = 'home';

  constructor(public router: Router) { }

  ngOnInit() {
  }

  go(route: any){
    this.link = route[1];
    this.router.navigate(route);
  }

}
